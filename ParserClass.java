import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Class for parser
 * @author Geanina
 *
 */
public class ParserClass extends ScannerClass
{
	
	/**
	 * Function that creates the dot file and tree
	 * @return 
	 * @throws FileNotFoundException
	 */
	public static HashMap<String, String> parser() throws FileNotFoundException
		{
		
		HashMap<String, Integer> priority = new HashMap<String,Integer>();
		priority.put(":=", 1);
		priority.put("=", 1);
		priority.put("!=", 1);
		priority.put("<", 1);
		priority.put(">", 1);
		priority.put("<=", 1);
		priority.put(">=", 1);
		priority.put("*", 2);
		priority.put("mod", 2);
		priority.put("div", 2);
		priority.put("+", 3);
		priority.put("-", 3);
		priority.put("(", 4);
		priority.put(")", 4);
		
		HashMap<String,String> variableTypes = new HashMap<String,String>();
		
        try {
        	//read the token file
        	File file = new File("output.tok");
        	Scanner read=new Scanner(file);
        	
            //declare the ast file
			PrintWriter output = new PrintWriter("output.ast.dot");
			
			output.println("digraph t112Ast {");
			output.println("  ordering=out;");
			output.println("  node [shape = box, style = filled, fillcolor=\"white\"];");
			
			String[] tokens=new String[numTokens];
			int index=0;
			while(read.hasNext())
			{
				tokens[index]=read.next();
				index++;
			}
			
			//create the initial nodes
			Node<String> root = new Node<String>(tokens[0]);
			Node<String> decl=new Node<String>("decl list");
			Node<String> stmt= new Node<String>("stm list");
			root.addChild(decl);
			root.addChild(stmt);
			
			output.println("  n1 [label=\""+root.getData()+"\", shape=box]");
			output.println("  n2 [label=\"decl list\", shape=box]");
			output.println("  n1 -> n2");
			int n=2;
			
			//find all variables, add them to the tree and print them in the dot file
			for(int i=0;i<tokens.length;i++)
			{
				if(tokens[i].equals("VAR"))
				{
					String[] name=tokens[i+1].split("\\(");
					String[] var=name[1].split("\\)");
					decl.addChild(new Node<String>("var[0]"));
					n++;
					output.println("  n"+n+" [label=\"decl:'"+var[0]+"':"+tokens[i+3]+"\", shape=box]");
					output.println("  n2 -> n"+n);
					variableTypes.put(var[0], tokens[i+3]);
				}
			}
			n++;
			int nstmt=n;
			output.println("  n"+n+" [label=\"stmt list\", shape=box]");
			output.println("  n1 -> n"+n);
			n++;
			int beginIndex=0;
			for(int i=0;i<tokens.length;i++)
			{
				if(tokens[i].toLowerCase().equals("begin"))
				beginIndex=i;
			}
			
			int x=beginIndex+1;
			int z=x;
			while(z<tokens.length && !tokens[z].equals("END"))
			{
				
				while(x<tokens.length && !tokens[x].equals("SC"))
				{
					
					String[] exp=tokens[x].split("\\(");
					String type=exp[0];
					
					
					String[] var = null;
					if(exp.length>1)
					var=exp[1].split("\\)");
					
					if(type.toLowerCase().equals("ident"))
					{
						if(tokens[x+1].equals("ASGN"))
						{
							//assign sign
							stmt.addChild(new Node<String>(":="));
							output.println("  n"+n+" [label=\":=\", shape=box]");
							output.println("  n"+nstmt+" -> n"+n);
							
							
							//first variable
							stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(var[0]+":int"));
							output.println("  n"+(n+1)+" [label=\""+var[0]+":int\", shape=box]");
							output.println("  n"+n+" -> n"+(n+1));
							
							
							x=x+2;
							if(tokens[x].equals("READINT"))
							{
								stmt.addChild(new Node<String>(":=")).addChild(new Node<String>("readint:int"));
								output.println("  n"+(n+2)+" [label=\"readint:int\", shape=box]");
								output.println("  n"+n+" -> n"+(n+2));
								n=n+3;
							}
							else
							
								if(tokens[x+1].equals("SC"))
								{
									exp=tokens[x].split("\\(");
									type=exp[0];
									if(exp.length>1)
									var=exp[1].split("\\)");
									if(type.toLowerCase().equals("ident"))
									{
										stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(var[0]+":int"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
									else if(type.toLowerCase().equals("num"))
									{
										stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(var[0]+":int"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
									else if(type.toLowerCase().equals("bool"))
									{
										stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(var[0]+":bool"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":bool\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
								}
								else
								{
									 exp=tokens[x+1].split("\\(");
									 type=exp[0];
									 if(exp.length>1)
									var=exp[1].split("\\)");
									int y=x;
									while(!tokens[y].equals("SC"))
									{
										int nSign=0;
										if((priority.containsKey(var[0])))
										{
											if(type.toLowerCase().equals("ident"))

											{
												stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int"));
												output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": int\", shape=box]");
												output.println("  n"+n+" -> n"+(n+2));
												nSign=n+2;
												n++;
												stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":int"));
												output.println("  n"+n+" [label=\""+var[0]+":int\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
												stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":int"));
												output.println("  n"+n+" [label=\""+tokens[y+2]+":int\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
											}
											else
												if(type.toLowerCase().equals("bool"))
												{
													stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": bool"));
													output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": bool\", shape=box]");
													output.println("  n"+n+" -> n"+(n+2));
													nSign=n+2;
													n++;
													stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":bool"));
													output.println("  n"+n+" [label=\""+var[0]+":bool\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
													stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":bool"));
													output.println("  n"+n+" [label=\""+tokens[y+2]+":bool\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
												}
												else
													if(type.toLowerCase().equals("num"))
													{
														stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int"));
														output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": int\", shape=box]");
														output.println("  n"+n+" -> n"+(n+2));
														nSign=n+2;
														n++;
														stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":int"));
														output.println("  n"+n+" [label=\""+var[0]+":int\", shape=box]");
														output.println("  n"+nSign+" -> n"+n);
														n++;
														stmt.addChild(new Node<String>(":=")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":int"));
														output.println("  n"+n+" [label=\""+tokens[y+2]+":int\", shape=box]");
														output.println("  n"+nSign+" -> n"+n);
														n++;
													}
											
										}
										y=y+2;
									
									
										
									}
								}
							}
						x=x+1;
						}
						
								
				
					
					else if(type.toLowerCase().equals("writeint"))
					{
						
						stmt.addChild(new Node<String>("writeint"));
						output.println("  n"+n+" [label=\"writeint\", shape=box]");
						output.println("  n"+nstmt+" -> n"+n);
						n++;
						
						if(tokens[x+2].equals("ASGN"))
						{
							//assign sign
							stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(":="));
							output.println("  n"+n+" [label=\":=\", shape=box]");
							output.println("  n"+nstmt+" -> n"+n);
							
							
							//first variable
							stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int"));
							output.println("  n"+(n+1)+" [label=\""+var[0]+":int\", shape=box]");
							output.println("  n"+n+" -> n"+(n+1));
							
							
							x=x+2;
							if(tokens[x].equals("READINT"))
							{
								stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>("readint:int"));
								output.println("  n"+(n+2)+" [label=\"readint:int\", shape=box]");
								output.println("  n"+n+" -> n"+(n+2));
								n=n+3;
							}
							else
							
								if(tokens[x+1].equals("SC"))
								{
									exp=tokens[x].split("\\(");
									type=exp[0];
									if(exp.length>1)
									var=exp[1].split("\\)");
									if(type.toLowerCase().equals("ident"))
									{
										stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
									else if(type.toLowerCase().equals("num"))
									{
										stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
									else if(type.toLowerCase().equals("bool"))
									{
										stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":bool"));
										output.println("  n"+(n+2)+" [label=\""+var[0]+":bool\", shape=box]");
										output.println("  n"+n+" -> n"+(n+2));
										n=n+3;
									}
								}
								else
								{
									 exp=tokens[x+1].split("\\(");
									 type=exp[0];
									var=exp[1].split("\\)");
									int y=x;
									while(!tokens[y].equals("SC"))
									{
										int nSign=0;
										
										if((priority.containsKey(var[0])))
										{
											if(type.toLowerCase().equals("ident"))

											{
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int"));
												output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": int\", shape=box]");
												output.println("  n"+n+" -> n"+(n+2));
												nSign=n+2;
												n++;
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":int"));
												output.println("  n"+n+" [label=\""+var[0]+":int\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":int"));
												output.println("  n"+n+" [label=\""+tokens[y+2]+":int\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
											}
											else
												if(type.toLowerCase().equals("bool"))
												{
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": bool"));
													output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": bool\", shape=box]");
													output.println("  n"+n+" -> n"+(n+2));
													nSign=n+2;
													n++;
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":bool"));
													output.println("  n"+n+" [label=\""+var[0]+":bool\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":bool"));
													output.println("  n"+n+" [label=\""+tokens[y+2]+":bool\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
												}
												else
													if(type.toLowerCase().equals("num"))
													{
														stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int"));
														output.println("  n"+(n+2)+" [label=\""+tokens[y+1]+": int\", shape=box]");
														output.println("  n"+n+" -> n"+(n+2));
														nSign=n+2;
														n++;
														stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(var[0]+":int"));
														output.println("  n"+n+" [label=\""+var[0]+":int\", shape=box]");
														output.println("  n"+nSign+" -> n"+n);
														n++;
														stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(tokens[y+1]+": int")).addChild(new Node<String>(tokens[y+2]+":int"));
														output.println("  n"+n+" [label=\""+tokens[y+2]+":int\", shape=box]");
														output.println("  n"+nSign+" -> n"+n);
														n++;
													}
											
										}
										y=y+2;
									
									
										
									}
								}
							}
						
						else
							if(tokens[x+2].equals("SC"))
							{
								exp=tokens[x].split("\\(");
								type=exp[0];
								if(exp.length>1)
								var=exp[1].split("\\)");
								if(type.toLowerCase().equals("ident"))
								{
									stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int"));
									output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
									output.println("  n"+n+" -> n"+(n+2));
									n=n+3;
								}
								else if(type.toLowerCase().equals("num"))
								{
									stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int"));
									output.println("  n"+(n+2)+" [label=\""+var[0]+":int\", shape=box]");
									output.println("  n"+n+" -> n"+(n+2));
									n=n+3;
								}
								else if(type.toLowerCase().equals("bool"))
								{
									stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":bool"));
									output.println("  n"+(n+2)+" [label=\""+var[0]+":bool\", shape=box]");
									output.println("  n"+n+" -> n"+(n+2));
									n=n+3;
								}
							}
							else
							{
								
								 exp=tokens[x+2].split("\\(");
								 
								 String[] exp2=tokens[x+1].split("\\(");
								 type=exp2[0];
								 if(exp.length>1)
								var=exp[1].split("\\)");
								int y=x;
								while(y+1<tokens.length && !tokens[y].equals("SC") && !tokens[y].equals("END") && !tokens[y+2].equals("SC") && !tokens[y+2].equals("END"))
								{
									int nSign=0;
									
									if((priority.containsKey(var[0])))
									{
										if(type.toLowerCase().equals("ident"))

										{
											nSign=n;
											stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int"));
											output.println("  n"+nSign+" [label=\""+var[0]+":int\", shape=box]");
											output.println("  n"+(n-1)+" -> n"+nSign);
											
											n++;
											exp2=exp2[1].split("\\)");
											stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int")).addChild(new Node<String>(exp2[0]+": int"));
											output.println("  n"+n+" [label=\""+exp2[0]+": int\", shape=box]");
											output.println("  n"+nSign+" -> n"+n);
											n++;
											
											
											String[] t=tokens[y+3].split("\\(");
											
											if(t.length>1)
												 t=t[1].split("\\)");
											stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int")).addChild(new Node<String>(t[0]+":int"));
											output.println("  n"+n+" [label=\""+t[0]+":int\", shape=box]");
											output.println("  n"+nSign+" -> n"+n);
											n++;
										}
										else
											if(type.toLowerCase().equals("bool"))
											{
												nSign=n;
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int"));
												output.println("  n"+nSign+" [label=\""+var[0]+":bool\", shape=box]");
												output.println("  n"+(n-1)+" -> n"+nSign);
												
												n++;
												exp2=exp2[1].split("\\)");
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int")).addChild(new Node<String>(exp2[0]+": int"));
												output.println("  n"+n+" [label=\""+exp2[0]+": bool\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
												
												
												String[] t=tokens[y+3].split("\\(");
												
												if(t.length>1)
													 t=t[1].split("\\)");
												stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int")).addChild(new Node<String>(t[0]+":int"));
												output.println("  n"+n+" [label=\""+t[0]+":bool\", shape=box]");
												output.println("  n"+nSign+" -> n"+n);
												n++;
											}
											else
												if(type.toLowerCase().equals("num"))
												{
													nSign=n;
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int"));
													output.println("  n"+nSign+" [label=\""+var[0]+":int\", shape=box]");
													output.println("  n"+(n-1)+" -> n"+nSign);
													
													n++;
													if(exp2.length>1)
													exp2=exp2[1].split("\\)");
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+":int")).addChild(new Node<String>(exp2[0]+": int"));
													output.println("  n"+n+" [label=\""+exp2[0]+": int\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
													
													
													String[] t=tokens[y+3].split("\\(");
													
													if(t.length>1)
														 t=t[1].split("\\)");
													stmt.addChild(new Node<String>("writeint")).addChild(new Node<String>(var[0]+": int")).addChild(new Node<String>(t[0]+":int"));
													output.println("  n"+n+" [label=\""+t[0]+":int\", shape=box]");
													output.println("  n"+nSign+" -> n"+n);
													n++;
												}
										
									}
									y=y+2;
								
								
									
								}
							}
						x=x+1;
							}
					x=x+1;
				}
				x=x+1;
				z=z+1;
			}
			read.close();
			output.println("}");
			output.close();
			System.out.println("Parser Succesfull!");
			System.out.println("AST.DOT File created.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		System.out.println("Parser Error");
		}
        return variableTypes;
}
		}
	
		
