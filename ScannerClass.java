import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This class reads the TL file and creates the TOK file
 * @author Geanina Tambaliuc
 *
 */
public class ScannerClass {
	 //variable to count the number of tokens in the file
    static int numTokens;
	public static void scanner() throws IOException {
		//create a HashMap for the keywords
		HashMap<String,String> keywords=new HashMap<String, String>();
		
		//add keywords to the HashMap
		keywords.put("if","IF");
		keywords.put("then","THEN");
		keywords.put("else","ELSE");
		keywords.put("begin","BEGIN");
		keywords.put("end","END");
		keywords.put("while","WHILE");
		keywords.put("do","DO");
		keywords.put("program","PROGRAM");
		keywords.put("var","VAR");
		keywords.put("as","AS");
		keywords.put("int","INT");
		keywords.put("bool","BOOL");
		keywords.put("writeInt","WRITEINT");
		keywords.put("readInt","READINT");
		keywords.put("writeint","WRITEINT");
		keywords.put("readint","READINT");
		keywords.put("(","LP");
		keywords.put(")","RP");
		keywords.put(":=","ASGN");
		keywords.put(";","SC");
		
		// create a HashMap for operators, numbers, literals, and identifiers
		HashMap<String,String> operatorsType = new HashMap<String,String>();
		
		operatorsType.put("*", "MULTIPLICATIVE");
		operatorsType.put("div", "MULTIPLICATIVE");
		operatorsType.put("mod", "MULTIPLICATIVE");
		operatorsType.put("+", "ADDITIVE");
		operatorsType.put("-", "ADDITIVE");
		operatorsType.put("=", "COMPARE");
		operatorsType.put("!=", "COMPARE");
		operatorsType.put("<", "COMPARE");
		operatorsType.put(">", "COMPARE");
		operatorsType.put("<=", "COMPARE");
		operatorsType.put(">=", "COMPARE");
		operatorsType.put("false", "boollit");
		operatorsType.put("true", "boollit");
		
		//get the name file from the user
		
		System.out.print("Enter the file name with extension : ");
		
        Scanner in = new Scanner(System.in);
        File file = new File(in.nextLine());
        System.out.println("Starting Scanning...");
        //declare the output file
        PrintWriter output = new PrintWriter("output.tok");
        
       
        
        //handle the FileNotFoundException
	    try {
	    	
			Scanner read=new Scanner(file);
			while(read.hasNext())
			{
				//read the file word by word
				String key=read.next();
				String value=keywords.get(key);
				if(value==null) //check if the word exist in the keywords map
				{
					value=operatorsType.get(key);  //if the word is not in keywords, then check operatorsType
					if(value==null)
					{
						if(isNum(key)==false)   //if the word is not in operatorsType, check if key is a number
						{
							//if it is not a number then it is a variable
							output.println("ident("+key+")");
							numTokens++;
							
						}
						else
						{
							//it's a number
							output.println("num("+key+")");
							numTokens++;
						}
					}
					else
					{
						output.println(value+"("+key+")");
						numTokens++;
					}
				}
				else
				{
					output.println(value);
					numTokens++;
				}
			}
			System.out.println("Scanner Succesfull!");
			System.out.println("The output (token) file has been created!");
			read.close();
			output.close();
			in.close();
			} catch (Exception e) {
			System.out.println("Scanner Error");
		}
	    
	    
		
	}
	

	/**
	 * This functions checks if a given is string is numerical or not
	 * Also checks if the number is between 0 and 2147483647
	 * @param str
	 * @return true/false
	 */
	public static boolean isNum(String str)
	{
		double num;
		try
		{
			//try to convert the string to double
			num=Double.parseDouble(str);
			
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		
		//check if it's in the right range
		if(num>=0 && num<=2147483647)
			return true;
		else return false;

	}
	
}
