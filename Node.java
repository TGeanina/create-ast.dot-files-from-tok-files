import java.util.ArrayList;
import java.util.List;

/**
 * This class creates all functions for a Node.
 * Used to create the tree.
 * @author Geanina Tambaliuc
 *
 * @param <T>
 */
public class Node<T> {
 
	
 private T data = null; //variable for node data
 private T type = null; //variable for var type
 private Node<T> parent = null; //variable for parent node
 private List<Node<T>> children = new ArrayList<>(); //variable for children nodes
 
 /**
  * Constructor
 * @param data
 */
public Node(T data) {
 this.data = data;
 }
/**
 * Setter for parent
* @param parent
*/
private void setParent(Node<T> parent) {
this.parent = parent;
}

/**
 * Getter for parent
* @return parent
*/
public Node<T> getParent() {
return parent;
}

 /**
  *  Function for adding a child node
 * @param child
 * @return child
 */

public Node<T> addChild(Node<T> child) {
 child.setParent(this);
 this.children.add(child);
 return child;
 }
 

 /**
  * Function to add a list of children nodes
 * @param children
 */
public void addChildren(List<Node<T>> children) {
 children.forEach(each -> each.setParent(this));
 this.children.addAll(children);
 }


 
 /**
  * Getter for children
 * @return children
 */
public List<Node<T>> getChildren() {
 return children;
 }
 

 /**
  * Getter for data
 * @return data
 */
public T getData() {
 return data;
 }
 
 /**
  * Setter for data
 * @param data
 */
public void setData(T data) {
 this.data = data;
 }
 
/**
 * Getter for root
 * @return root
 */
public Node<T> getRoot() {
	 if(parent == null){
	  return this;
	 }
	 return parent.getRoot();
	}

}